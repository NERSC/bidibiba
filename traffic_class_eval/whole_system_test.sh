#!/bin/bash

#OpenMP settings:
export OMP_NUM_THREADS=1
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

export MPICH_VERSION_DISPLAY=ENABLE
export LD_LIBRARY_PATH=/opt/cray/pe/mpich/8.1.25/ofi/cray/10.0/lib:/opt/cray/pe/mpich/8.1.25/gtl/lib:$LD_LIBRARY_PATH
export MPICH_OFI_NIC_POLICY=NUMA
export MPICH_OFI_NUM_NICS=4

basedir="${PSCRATCH}/bidibiba"
hostlistA="${basedir}/${SLURM_JOB_ID}.hostlistA"
hostlistB="${basedir}/${SLURM_JOB_ID}.hostlistB"
#split the nodes into two sets to run tests simultaneously
n=$(($SLURM_NNODES / 2))
scontrol show hostnames | head -${n} > ${hostlistA}
scontrol show hostnames | tail -${n} > ${hostlistB}
#Four NICS per node on GPUs
p=$(($n * 4))


#Best effort should be 50% allocation guaranttee, but both runs use it and should be about equal.
export MPICH_OFI_DEFAULT_TCLASS=TC_BEST_EFFORT
#run the application:
srun --export=ALL  --nodes=${n} --ntasks=${p} --nodefile=${hostlistA} --ntasks-per-node=4 --cpu-bind=map_cpu:48,32,16,0 --cpu-freq=high ${basedir}/bidibiba_ss > ${basedir}/bidiA.1 &
#Try having the second run in a different class
#export export MPICH_OFI_DEFAULT_TCLASS=TC_SCAVENGER
echo $MPICH_OFI_DEFAULT_TCLASS
srun --export=ALL --nodes=${n} --ntasks=${p} --nodefile=${hostlistB} --ntasks-per-node=4 --cpu-bind=map_cpu:48,32,16,0 --cpu-freq=high ${basedir}/bidibiba_ss > ${basedir}/bidiB.1

#Bulk data has a 20% allocation of bandwidth and we should see that bidiB.1 has less throughput than bidiA.1
echo $MPICH_OFI_DEFAULT_TCLASS
#run the application:
srun --export=ALL --nodes=${n} --ntasks=${p} --nodefile=${hostlistA} --ntasks-per-node=4 --cpu-bind=map_cpu:48,32,16,0 --cpu-freq=high ${basedir}/bidibiba_ss > ${basedir}/bidiA.2 &
export MPICH_OFI_DEFAULT_TCLASS=TC_BULK_DATA
echo $MPICH_OFI_DEFAULT_TCLASS
srun --export=ALL --nodes=${n} --ntasks=${p} --nodefile=${hostlistB} --ntasks-per-node=4 --cpu-bind=map_cpu:48,32,16,0 --cpu-freq=high ${basedir}/bidibiba_ss > ${basedir}/bidiB.2

